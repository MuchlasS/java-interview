package com.interview.controllers;

import java.util.List;

import com.interview.models.Product;
import com.interview.models.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    // get all product
    @GetMapping("/")
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    //  get single product
    @GetMapping("/{id}")
    public Product getSingleProduct(@PathVariable Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    // add product
    @PostMapping
    public Product addProduct(@RequestBody Product product){
        return productRepository.save(product);
    }

    // update product
    @PutMapping
    public Product updateProduct(@RequestBody Product product){
        Product oldProduct = productRepository.findById(product.getId()).orElse(null);
        oldProduct.setName(product.getName());
        oldProduct.setDescription(product.getDescription());
        oldProduct.setPrice(product.getPrice());

        return productRepository.save(oldProduct);
    }

    // delete product
    @DeleteMapping("/{id}")
    public Integer deleteProduct(@PathVariable Integer id){
        productRepository.deleteById(id);
        return id;
    }
}
